package com.example.recyclerviewpractice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MahasiswaAdapter.RecyclerViewCallBacks{

    private RecyclerView recyclerView;
    private MahasiswaAdapter adapter;
    private ArrayList<Mahasiswa> mahasiswaArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addData();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new MahasiswaAdapter(this, mahasiswaArrayList);
        RecyclerView.LayoutManager
                layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    void addData(){
        mahasiswaArrayList = new ArrayList<>();
        mahasiswaArrayList.add(new Mahasiswa("Dimas Maulana", "1414370309", "123456789"));
        mahasiswaArrayList.add(new Mahasiswa("Fadly Yonk", "1214234560", "987654321"));
        mahasiswaArrayList.add(new Mahasiswa("Ariyandi Nugraha", "1214230345", "987648765"));
        mahasiswaArrayList.add(new Mahasiswa("Aham Siswana", "1214378098", "098758124"));
        mahasiswaArrayList.add(new Mahasiswa("Rahadi Jalu", "235121344", "0812345355"));
        mahasiswaArrayList.add(new Mahasiswa("Muhammad Luqman", "123445560", "099877384857"));
        mahasiswaArrayList.add(new Mahasiswa("Isfan Fauzi", "7879230345", "548586723"));
        mahasiswaArrayList.add(new Mahasiswa("Geri Yesa", "00904378098", "928986872"));
        mahasiswaArrayList.add(new Mahasiswa("Budi Suryo", "20193832903", "091283394"));
        mahasiswaArrayList.add(new Mahasiswa("Rabi Dangan", "97410293842", "724103982"));
        mahasiswaArrayList.add(new Mahasiswa("Tupi Roro", "83920138399", "238290384"));
    }

    @Override
    public void onRecyclerviewItemSelected(String item) {
        Toast.makeText(this, "Anda memilih si " + item, Toast.LENGTH_LONG).show();
    }
}