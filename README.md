> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 12

1. Kegiatan Praktikum
        >![Recycler](/images/Recycler.png "Recycler")

1. Penugasan
        >![Toast](/images/Toast.png "Toast")
        >![Toast 2](/images/Toast2.png "Toast 2")
        >![Toast 3](/images/Toast3.png "Toast 3")
        Setelah ditambahkan toast, ketika kita menekan salah satu dari daftar mahasiswa akan muncul pop up yang bertuliskan "Anda memilih si ... " yang mana itu tergantung dari siapa yang kita pilih. Sistem akan otomatis mengambil kata pertama dari nama mahasiswa untuk ditampilkan pada toast. 
        